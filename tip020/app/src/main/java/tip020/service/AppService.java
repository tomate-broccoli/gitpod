package tip020.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tip020.dao.AppMapper;

@Service
public class AppService {
    @Autowired
    AppMapper mapper;
   
    public String getText(){
        return mapper.getText();
    }
}
