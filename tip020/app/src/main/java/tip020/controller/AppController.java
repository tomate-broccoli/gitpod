package tip020.controller;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tip020.service.AppService;

@RestController
public class AppController {

    @Autowired
    AppService srv;

    @RequestMapping("/")
    String home(){
        return srv.getText();
    }

}
