package tip020;

import org.springframework.boot.SpringApplication;
// import org.springframework.context.ConfigurableApplicationContext;
// import org.springframework.stereotype.Component;

// @Component
public class AppMain {

    public static void main(String[] args){
//        try(ConfigurableApplicationContext ctx = SpringApplication.run(AppConfig.class, args)){
//            AppMain main = ctx.getBean(AppMain.class);
//            main.run(args);
//        }
        SpringApplication.run(AppConfig.class, args);
    }

}