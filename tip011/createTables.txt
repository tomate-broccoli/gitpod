-- 
create table EMP (
   id varchar2(10),
   name varchar2(40)
);
insert into EMP values('e001', '青木');
insert into EMP values('e002', '井上');
insert into EMP values('e003', '内田');
insert into EMP values('e004', '江藤');
insert into EMP values('e005', '小田');

create table DEPT (
   id varchar(10),
   name varchar2(40)
);
insert into DEPT values('d001', '営業');
insert into DEPT values('d002', '経理');
insert into DEPT values('d003', '総務');
_
create table POS (
   id varchar(10),
   name varchar2(40)
);
insert into POS values('p001', '社長');
insert into POS values('p002', '専務');
insert into POS values('p003', '部長');

create table EMP_DEPT(
   emp_id varchar(10),
   dept_id varchar(10)
);
insert into EMP_DEPT values('e002', 'd001');
insert into EMP_DEPT values('e003', 'd002');
insert into EMP_DEPT values('e003', 'd003');
insert into EMP_DEPT values('e004', 'd001');

create table EMP_POS (
   emp_id varchar(10),
   pos_id varchar(10)
);
insert into EMP_POS values('e001', 'p001');
insert into EMP_POS values('e002', 'p002');
insert into EMP_POS values('e002', 'p003');
insert into EMP_POS values('e003', 'p003');

create table DEPT_POS (
   dept_id varchar(10),
   pos_id varchar(10)
);
insert into DEPT_POS values('d001', 'p003');
insert into DEPT_POS values('d002', 'p003');
insert into DEPT_POS values('d003', 'p003');
commit;

--
with wk001 as (
  select d.emp_id, f.dept_id, f.pos_id
  from dept_pos f
  inner join emp_dept d on d.dept_id = f.dept_id 
  inner join emp_pos  e on e.pos_id  = f.pos_id
                       and e.emp_id  = d.emp_id
)
,wk002 as (
        select d.emp_id, d.dept_id from emp_dept d
  minus select h.emp_id, h.dept_id from wk001 h
 )
,wk003 as (
        select e.emp_id, e.pos_id from emp_pos e
  minus select h.emp_id, h.pos_id from wk001 h
 )
,wk004 as (
        select a.id as emp_id from emp a
  minus select h.emp_id       from wk001 h
  minus select i.emp_id       from wk002 i
  minus select j.emp_id       from wk003 j
)
      select emp_id, dept_id, pos_id from wk001
union select emp_id, dept_id, null   from wk002
union select emp_id, null,    pos_id from wk003
union select emp_id, null,    null   from wk004
order by 1, 2 nulls first, 3 nulls first
;