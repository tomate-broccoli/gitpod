# tip026

##  vscodevimが動かない.
- 2024.11.17
- Gitpod Web v0.0.7
- vscode vim v.1.28.1
- vs code v1.95.0 browser

## memo
- 以前（１ヶ月くらい前？ 2024.10頃？)は問題なくvim使えてた。
-今月(2024.11)から動かなくなった。
- vscode vimのバージョンのせい？
  - v1.27.2でもダメだった...
- vscodeのバージョンのせい？
  - いろいろ試したがダメだった...
- gitpodのせい？

__END__

